function Renderer() {
    function getById(id) {
        return document.getElementById(id).textContent;
    }
    
    var options = { minFilter: gl.LINEAR_MIPMAP_LINEAR, wrap: gl.REPEAT, format: gl.RGB};
    
    this.tessellationTexture = GL.Texture.fromImage(document.getElementById("tessellation"), options);
    this.causticsTexture = new GL.Texture(1024, 1024);
    
    options.minFilter = gl.LINEAR;
    options.type = gl.FLOAT;
    options.wrap = gl.CLAMP_TO_EDGE;
    options.format = gl.RGBA;
    
    this.waterTextures = []; 
    this.waterTextures.push(new GL.Texture(256, 256, { type: gl.FLOAT, filter: gl.LINEAR }));
    this.waterTextures.push(new GL.Texture(256, 256, { type: gl.FLOAT, filter: gl.LINEAR }));
    
    gl.getExtension('OES_standard_derivatives');
    this.cubeShader = new GL.Shader(getById('cubeVertexShader'), getById('cubeFragmentShader'));
    this.causticsShader = new GL.Shader(getById('causticsVertexShader'), getById('causticsFragmentShader'));
    this.aboveWaterShader = new GL.Shader(getById('waterVertexShader'), getById('aboveWaterFragmentShader'));
    this.underWaterShader = new GL.Shader(getById('waterVertexShader'), getById('underWaterFragmentShader'));
    this.dropShader = new GL.Shader(getById('waveVertexShader'), getById("dropFragmentShader"));
    this.updateShader = new GL.Shader(getById('waveVertexShader'), getById("updateFragmentShader"));
    this.normalShader = new GL.Shader(getById('waveVertexShader'), getById('normalFragmentShader'));
    this.skyShader = new GL.Shader(getById('skyVertexShader'), getById('skyFragmentShader'));
    
    this.rayMesh = GL.Mesh.sphere({detail: 1});
    
    this.skyMesh = GL.Mesh.sphere({detail: 50});
    var scaleToWorld = new GL.Matrix();
    GL.Matrix.translate(0, 0, 0, scaleToWorld);
    GL.Matrix.scale(3, 3, 3, scaleToWorld);
    this.skyMesh.transform(scaleToWorld);
    this.skyMesh.compile();
    
    this.cubeMesh = GL.Mesh.cube();
    // remove the roof
    this.cubeMesh.triangles.splice(4, 2);
    var scaleMatrix = new GL.Matrix();
    GL.Matrix.scale(2, 1, 2, scaleMatrix);
    //this.cubeMesh.transform(scaleMatrix);
    this.cubeMesh.compile();
    
    this.waterMesh = GL.Mesh.plane({detailX: 200, detailY: 200});
    var scaleMatrix2 = new GL.Matrix();
    GL.Matrix.scale(2, 2, 1, scaleMatrix2);
    //this.waterMesh.transform(scaleMatrix2);
    this.waterMesh.compile();
    
    this.waveMesh = GL.Mesh.plane();
    //this.waveMesh.transform(scaleMatrix2);
    
    this.sphereCenter = new GL.Vector(0, 0, 0);
    this.sphereRadius = 0.1;
}
Renderer.prototype.renderCube = function() {
    gl.enable(gl.CULL_FACE);
    this.waterTextures[0].bind(0);
    this.tessellationTexture.bind(1);
    this.causticsTexture.bind(2);
    this.cubeShader.uniforms({
        rayDir: rayDir,
        water: 0,
        tessellation: 1,
        causticTex: 2
    }).draw(this.cubeMesh);
    gl.disable(gl.CULL_FACE);
    return this;
}
Renderer.prototype.renderWater = function() {
    this.waterTextures[0].bind(0);
    this.tessellationTexture.bind(1);
    sky.bind(2);
    this.causticsTexture.bind(3);
    var tracer = new GL.Raytracer();
    gl.enable(gl.CULL_FACE);
    gl.cullFace(gl.FRONT);
    this.aboveWaterShader.uniforms({
        rayDir: rayDir,
        water: 0,
        tessellation: 1,
        sky: 2,
        causticTex: 3,
        eye: tracer.eye
    }).draw(this.waterMesh);
    gl.cullFace(gl.BACK);
    this.underWaterShader.uniforms({
        rayDir: rayDir,
        water: 0,
        tessellation: 1,
        sky: 2,
        causticTex: 3,
        eye: tracer.eye
    }).draw(this.waterMesh);
    gl.disable(gl.CULL_FACE);
    return this;
}
Renderer.prototype.renderCaustics = function() {
    this.causticsTexture.drawTo(function() {
        gl.clear(gl.COLOR_BUFFER_BIT);
        renderer.waterTextures[0].bind(0);
        renderer.causticsShader.uniforms({
            rayDir: rayDir,
            water: 0
        }).draw(renderer.waterMesh);
    });
    return this;
}
Renderer.prototype.createDrop = function(x, y, radius, strength) {
    this.waterTextures[1].drawTo(function() {
        renderer.waterTextures[0].bind(0);
        renderer.dropShader.uniforms({
            center: [x, y],
            texture: 0,
            radius: radius,
            strength: strength
        }).draw(renderer.waveMesh);
    });
    this.waterTextures[1].swapWith(this.waterTextures[0]);
    return this;
}
Renderer.prototype.updateNormals = function() {
    this.waterTextures[1].drawTo(function() {
        renderer.waterTextures[0].bind(0);
        renderer.normalShader.uniforms({
            delta: [1 / renderer.waterTextures[0].width, 1 / renderer.waterTextures[0].height],
            texture: 0
        }).draw(renderer.waveMesh);
    });
    this.waterTextures[1].swapWith(this.waterTextures[0]);
    return this;
}
Renderer.prototype.updateFrame = function() {
    this.waterTextures[1].drawTo(function() {
        renderer.waterTextures[0].bind(0);
        renderer.updateShader.uniforms({
            delta: [1 / renderer.waterTextures[0].width, 1 / renderer.waterTextures[0].height],
            texture: 0
        }).draw(renderer.waveMesh);
    });
    this.waterTextures[1].swapWith(this.waterTextures[0]);
    return this;
}
Renderer.prototype.renderSky = function() {
    gl.enable(gl.CULL_FACE);
    sky.bind(0);
    this.skyShader.uniforms({
        sky: 0
    }).draw(this.skyMesh);
    gl.disable(gl.CULL_FACE);
    return this;
}
