window.onload = setup;
window.onresize = resize;
var gl = GL.create();
var sky, cube, renderer, skys = [];
var angle = [-25, -135.5], rayDir = new GL.Vector(-2.0, 1.6, 1.0).unit(), oldX = 0, oldY = 0, moveCam = false, createDrop = false;
var rainFlag = false;
var frameCount = 0, lastTime = 0, elapsedTime = 0;
function setup() {
    document.body.appendChild(gl.canvas);
    lastTime = new Date().getTime();
    gl.clearColor(0, 0, 0, 1);
    sky = new SkyBox();
    skys.push(sky);
    skys.push(new SkyBox("+"));
    renderer = new Renderer();
    rainning();
    resize();
    window.requestAnimationFrame(runner);
    document.onkeydown = keyEventHandler;
}
function rainning() {
    if (rainFlag) {
        sky = skys[1];
        for (var i = 0; i < 4; i++)
            renderer.createDrop(Math.random() * 2 - 1, Math.random() * 2 - 1, 0.03, (i & 1) ? 0.05 * Math.random() : -0.05 * Math.random());
    } else
        sky = skys[0];
    setTimeout(rainning, 0.4);
}
function keyEventHandler(e) {
    if (e.which == 'R'.charCodeAt(0))
        rainFlag = !rainFlag;
}
function runner() {
    renderer.updateFrame();
    renderer.updateFrame();
    renderer.updateNormals();
    renderer.renderCaustics();
    animate();
    window.requestAnimationFrame(runner);
    var now = new Date().getTime();
    frameCount++;
    elapsedTime += now - lastTime;
    lastTime = now;
    if (elapsedTime >= 1000) {
        var fps = frameCount;
        frameCount = 0;
        elapsedTime -= 1000;
        document.getElementById("fps").innerHTML = fps;
    }
};
function resize() {
    gl.canvas.width = window.innerWidth;
    gl.canvas.height = window.innerHeight;
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.matrixMode(gl.PROJECTION);
    gl.loadIdentity();
    gl.perspective(45, gl.canvas.width / gl.canvas.height, 0.01, 100);
    gl.matrixMode(gl.MODELVIEW);
    animate();
}
function animate() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.loadIdentity();
    gl.translate(0, 0, -4);
    gl.rotate(-angle[0], 1, 0, 0);
    gl.rotate(-angle[1], 0, 1, 0);
    gl.translate(0, 0.5, 0);

    gl.enable(gl.DEPTH_TEST);
    renderer.renderCube().renderWater().renderSky();
    gl.disable(gl.DEPTH_TEST);
}
document.onmousedown = function(e) {
    oldX = e.pageX;
    oldY = e.pageY;
    moveCam = true;
    var tracer = new GL.Raytracer();
    var ray = tracer.getRayForPixel(e.pageX, e.pageY);
    var hit = tracer.eye.add(ray.multiply(-tracer.eye.y / ray.y));
    if (Math.abs(hit.x) < 1 && Math.abs(hit.z) < 1) {
        moveCam = false;
        createDrop = true;
    }
}
document.onmousemove = function(e) {
    // Limit camera from backtrace
    if (moveCam) {
        angle[1] -= e.pageX - oldX;
        angle[0] -= e.pageY - oldY;
        angle[0] = Math.max(-89.999, Math.min(89.999, angle[0]));
        oldX = e.pageX;
        oldY = e.pageY;
    } else if (createDrop) {
        var tracer = new GL.Raytracer();
        var ray = tracer.getRayForPixel(e.pageX, e.pageY);
        var hit = tracer.eye.add(ray.multiply(-tracer.eye.y / ray.y));
        renderer.createDrop(hit.x, hit.z, 0.03, 0.1);
    }
    oldX = e.pageX;
    oldY = e.pageY;
}
document.onmouseup = function(e) {
    createDrop = moveCam = false;
}
